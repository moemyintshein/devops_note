# DevOps_note

<h4>POD Basic</h4>

Creating single POD without any controller

- kubectl -n NAMESPACE run PODNAME --image=nginx --restart=Never
- kubectl -n NAMESPACE get pod/deploy/rc/all     // review pod,deploy,rc of NAMESPACE
- kubectl describe pod -n NAMESPACE PODNAME
- kubectl -n NAMESPACE delete pod PODNAME       //delete pod
- kubectl -n NAMESPACE delete pod -l app=PODLABELS   //delete pod with labels name
- kubectl -n NAMESPACE logs PODNAME


Exposing as as Service(svc)

- kubectl -n NAMESPACE expose pod PODNAME --port=80 --protocol=TCP
- kubectl -n NAMESPACE get service/svc
- kubectl -n NAMESPACE get endpoints/ep
- kubectl -n NAMESPACE describe service PODNAME      //should be service labels = pod labels
- kubectl -n NAMESPACE get pod PODNAME --output yaml    //get pod's yaml
- kubectl -n NAMESPACE get svc -o yaml                  //get service's yaml
- kubectl -n NAMESPACE patch svc SERVICENAME -p '{"spec":{"type":"NodePort"}}     //change service's configuration 
- kubectl -n NAMESPACE get svc,pod,node -o wide        //NODE-IP + SERVICE-PORT   (get node IP , worker, service port/ nodePort = phisical host listen port, port = container port)
- 


Export POD template

- kubectl -n NAMESPACE get pod PODNAME -o yaml --export > filename.yaml
- kubectl -n NAMESPACE create -f mms-nginx.yaml     //create pod with yaml config
- 


<h4>Deploymant Basic</h4>

Create Deployment

- kubectl -n NAMESPACE create deploy NAME --image=nginx


- kubectl scale deployment NAME --replicas=3 -n NAMESPACE
 

- kubectl expose deploy NAME --port=80 --protocol=TCP -n NAMESPACE
- kubectl -n NAMESPACE edit svc SERVICENAME
- 

- kubectl edit deploy NAME -n NAMESPACE
- kubectl -n mms-app rollout history deployment DEPLOYMENTNAME
- kubectl -n mms-app rollout undo deploy web-app --to-revision=1


<h3>ConfigMap</h3>

- kubectl -n NAMESPACE create configmap index-map --from-file=index.html          //create configMap from yaml created file
- kubectl -n NAMESPACE get configmap
- kubectl -n NAMESPACE get configmap index-map -o yaml
- kubectl -n NAMESPACE delete cm index-map
- kubectl -n NAMESPACE apply -f web-app.yaml
- 


<h3>CI / CD </h3>

Git global setup

<pre>
git config --global user.name "Moe Myint Shein"
git config --global user.email "moemyintshein@outlook.com"
</pre>

Create a new repository

<pre>
git clone https://gitlab.com/ymb-study/moemyintshein.git
cd moemyintshein
touch README.md
git add README.md
git commit -m "add README"
</pre>


Push an existing folder

<pre>
cd existing_folder
git init
git remote add origin https://gitlab.com/ymb-study/moemyintshein.git
git add .
git commit -m "Initial commit"
git push -u origin master
</pre>


Push an existing Git repository

<pre>
cd existing_repo
git remote rename origin old-origin
git remote add origin https://gitlab.com/ymb-study/moemyintshein.git
</pre>